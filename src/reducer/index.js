import {combineReducers} from 'redux';
import todoReducer from './todoReducer'
import filterTodoReducer from './filterTodoReducer'
const allReducers = combineReducers({ todoReducer, filterTodoReducer  })
export default allReducers;