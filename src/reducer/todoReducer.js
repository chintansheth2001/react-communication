const todoReducer = ( state = [], action) => {
    switch(action.type){
        case 'ADD_TODO': {
            const {id, task} = action.payload
            const newState = [...state];
            newState.push({ id, task, completed: false});            
            return newState;
        }
        case 'TOGGLE_TODO': {
            const id  = action.payload;
            let newState = [];            
            newState = state.map( 
                todo => {
                    if(todo.id === id){
                        todo = Object.assign({ }, todo);
                        todo.completed =  !todo.completed;
                    }
                    return todo
                } 
            );
            return newState;
        }
        case  'DELETE_TODO' : {
            const id = action.payload
            let newState = []
            newState = state.filter( todo => todo.id !== id )
            return newState
        }

        default:
            return state
            
    }

}
export default todoReducer