import React from 'react';
import { connect } from "react-redux";
import ToDoItem from './ToDoItem';
import { toggleTodo, deleteTodo, filterTodo } from '../actions';
import getTodo from '../actions/getTodo';
import { 
    Button,
    Segment,
    Message,
    Grid,
} from 'semantic-ui-react'


function ToDoListBox( { todos, filterTodoStatus, toggleTodo, deleteTodo, filterTodo } ){
    return(
        <div>
        {
            todos && todos.length ?
            todos.map( (todo ,i) => <ToDoItem key={i} todo={todo} toggleTodo={toggleTodo}  deleteTodo={deleteTodo} /> )                
            :
            <Segment>
                <Message info>No todo yet</Message>
            </Segment>
        }
        
        <Segment> 
        <Grid columns={3} >
            <Grid.Row>                
                <Grid.Column className="textLeft"><Button  primary onClick={ () => { filterTodo('All') } }>All</Button></Grid.Column>
                <Grid.Column><Button  primary onClick={ () => { filterTodo('Done') } }>Completed</Button></Grid.Column>
                <Grid.Column className="textRight"><Button  primary onClick={ () => { filterTodo('Not Completed') } }>Not Completed</Button></Grid.Column>
            </Grid.Row>
        </Grid>
        </Segment>
        </div>
    )
}

const mapStateToProps = (state ) => {    
    const todos = getTodo(state.todoReducer, state.filterTodoReducer )
    const filterTodoStatus = state.filterTodoReducer;
    return { todos, filterTodoStatus };
}

const mapDispatchToProps = {
    toggleTodo,
    deleteTodo,
    filterTodo
}

export default connect(mapStateToProps, mapDispatchToProps)(ToDoListBox)