import React from 'react';
import { 
    Button,
    Segment,
    Grid
 } from 'semantic-ui-react'


function ToDoItem(props){
    const {todo, toggleTodo, deleteTodo} = props;
    const {id, task, completed} = todo;

    return(
        <Segment>
            <Grid  >
                <Grid.Row>
                    <Grid.Column className="textLeft" width={13} onClick={ () => {toggleTodo(id)} } style={{ textDecoration:  `${completed ? 'line-through' : 'none'}`  }}>
                        <div className="todo">{task}</div>
                    </Grid.Column>   
                    <Grid.Column className="textLeft" width={3}>
                        <Button color="red" onClick={ () => {deleteTodo(id)} }>Delete</Button>
                    </Grid.Column>     
                </Grid.Row>
            </Grid>  
        </Segment>
    )
}

export default ToDoItem;