import React, {Component} from 'react';
import {connect} from 'react-redux'
import {addTodo} from '../actions'
import { 
    Input,
    Button,
    Segment,
    Grid
} from 'semantic-ui-react'



class AddToDoBox extends Component{
    constructor(props) {
        super(props);
        this.state = {input: ''};
    }

    updateInput = input => {
        this.setState({ input });
    }

    handleAddTodo = () => {
        this.props.addTodo(this.state.input)
        this.setState({input: ''})
    }
    
    
    render(){        
        return(
            <Segment>
                <Grid  >
                    <Grid.Row>
                        <Grid.Column className="textLeft" width={13}>
                            <Input fluid value={this.state.input} 
                                placeholder=" Add Todo"
                                type="text" onChange={ e => this.updateInput(e.target.value) } /> 
                        </Grid.Column>
                        <Grid.Column className="textRight" width={3} ><Button primary onClick={this.handleAddTodo} >Add</Button></Grid.Column>
                    </Grid.Row>
                </Grid>
            </Segment>
        )
    }
}


export default connect(null, { addTodo } )(AddToDoBox);