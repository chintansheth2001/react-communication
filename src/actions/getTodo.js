const getTodo = ( store, filterValue ) => {
    
    if (filterValue === 'Done' ){
        return store.filter( todo => todo.completed === true )
    } 
    if (filterValue === 'Not Completed'){
        return store.filter( todo => todo.completed === false )
    }
    return store

}

export default getTodo