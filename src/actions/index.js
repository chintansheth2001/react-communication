let nextTodoId = 0
export const addTodo = task => {
    return {
        type: 'ADD_TODO',
        payload:{
            id: ++nextTodoId,
            task
        }
    }
}
export const toggleTodo = id => {
    return {
        type: 'TOGGLE_TODO',
        payload: id 
    }
}

export const deleteTodo = id => {
    return {
        type: 'DELETE_TODO',
        payload: id 
    }
}

export const filterTodo = filter => {
    return {
        type: 'FILTER_TODO',
        payload: filter 
    }
}
