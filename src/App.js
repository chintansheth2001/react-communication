import React, {Component} from 'react';
import './App.css';
import AddToDoBox from './component/AddToDoBox';
import ToDoListBox from './component/ToDoListBox';
import { 
  Container
} from 'semantic-ui-react'





class App extends Component {
  render(){    
    return (
      <Container className="App" style={{width: '600px'}}>
        <h1>Todo</h1>
        <AddToDoBox  />
        <ToDoListBox />
      </Container>
    );
  }
}

export default App;
